<?xml version="1.0" encoding="utf-8"?>
<chapter label="2" id="nonpkg">
    <title>Non-Package Development</title>
    <para>There are a number of ways to contribute to the development of Adélie Linux that do not involve packaging.  This chapter will introduce you to other Adélie Linux projects and how to set up a development environment for each one.</para>
    <section>
        <title>Developing Adélie Documentation</title>
        <para>In this section, you will learn:</para>
        <itemizedlist>
            <listitem><para>How documentation and guides like this one are authored; and</para></listitem>
            <listitem><para>How you can configure your environment to edit and author documentation for Adélie Linux.</para></listitem>
        </itemizedlist>
        <section>
            <title>Authorship of Documentation</title>
            <para>The Adélie Linux Documentation Set, including the guide that you are reading, is written in a format called DocBook.  DocBook is somewhat similar to HTML in that it uses <firstterm>tags</firstterm> to define the semantics of the document, but the tags DocBook uses are different from HTML's and focus on creation of books and manuals.</para>
            <para>The main Documentation Set is written in English, but we are happy to accept translations of our documentation into other languages.</para>
            <para>You can contribute corrections, additions, or translations of any portion of our documentation by emailing the adelie-docs mailing list with your patch, or by creating a merge request on the Documentation GitLab page.  For more information about using Git to create patches and merge requests, see chapter 3.</para>
        </section>
        <section>
            <title>Configuring Your Environment</title>
            <para>You will need to install the <package>xmlto</package> package to ensure your changes leave the documentation syntactically valid.  After making your changes, run the <command>xmlto</command> command on the primary file (for this guide, <filename>develguide.xml</filename>) and ensure there are no errors.</para>
        </section>
    </section>
    <section>
        <title>Spread the Word</title>
        <para>There are many ways to spread the word about Adélie Linux.
            Talk to your friends about Adélie Linux and the benefits of libre software, including security, speed, control, and ability to use the latest features without purchasing new computers every year.
            Have civil discussions on forums, newsgroups, and social media.
            If you're a student, talk to your school's Computer Science department and see if they are interested in a lab with Adélie Linux.
            By spreading the word, you're helping more people learn and spreading our ideas and goals worldwide.  Thank you!</para>
    </section>
</chapter>
