<?xml version="1.0" encoding="utf-8"?>
<chapter label="2" id="requirements">
    <title>System Requirements</title>
    <para>Ensure that your computer meets the following requirements before installing Adélie Linux.
        On Windows, you may run the <command>msinfo32</command> program to determine your computer's specifications.
        On a Macintosh, choose <guimenuitem>About This Mac</guimenuitem> from the <guimenu>Apple</guimenu> menu, and then choose <guibutton>System Report</guibutton> or <guibutton>More Info</guibutton>.
    </para>
    <section>
        <title>Intel x86 (32-bit)</title>
        <para>You will need at least a Pentium or Celeron compatible CPU, with MMX extensions.  This includes most AMD (K5+), Transmeta, VIA, and other x86-compatible CPUs.  Speeds of at least 500 MHz are highly recommended for running desktop applications, but speeds as low as 100 MHz may be usable in server mode.</para>
        <para>At least 256 MB RAM is required to install the desktop version of Adélie Linux, and at least 512 MB RAM is highly recommended if you plan to run larger applications like Firefox and Thunderbird.  At least 40 MB RAM is required to install the server version of Adélie Linux.</para>
        <warning><title>Warning</title><para>Using less than 40 MB RAM may cause lock ups or crashes during package installation.</para></warning>
        <para>At least 3 GB of disk space is required for a full desktop installation of Adélie Linux.  The base server installation of Adélie Linux requires about 300 MB of disk space, not counting additional packages and your data.  It is recommended that you have at least 1 GB of disk space for most server roles.</para>
    </section>
    <section>
        <title>Intel x86_64 (64-bit)</title>
        <para>All Intel CPUs that have 64-bit extensions are supported by the Adélie Linux kernel and software distribution.</para>
        <para>At least 256 MB RAM is required to install the desktop version of Adélie Linux, and at least 512 MB RAM is highly recommended if you plan to run larger applications like Firefox and Thunderbird.  At least 48 MB RAM is required to install the server version of Adélie Linux.</para>
        <warning><title>Warning</title><para>Using less than 48 MB RAM may cause lock ups or crashes during package installation.</para></warning>
        <para>At least 3 GB of disk space is required for a full desktop installation of Adélie Linux.  The base server installation of Adélie Linux requires about 270 MB of disk space, not counting additional packages and your data.  It is recommended that you have at least 1 GB of disk space for most server roles.</para>
    </section>
    <section>
        <title>PowerPC (32-bit)</title>
        <para>You will need at least a G3 CPU.  The 603 series is not supported by this version of Adélie Linux.  Speeds of at least 600 MHz are highly recommended for running desktop applications, but all G3 and G4 CPUs are supported.  AltiVec/VMX is not required to run Adélie, but some packages may perform much better on systems that have these extensions.</para>
        <para>At least 3 GB of disk space is required for a full desktop installation of Adélie Linux.  The base server installation of Adélie Linux requires about 200 MB of disk space, not counting additional packages and your data.  It is recommended that you have at least 1 GB of disk space for most server roles.</para>
    </section>
    <section>
        <title>PowerPC (64-bit)</title>
        <para>You will need a POWER4+, or 970/970fx, or later generation CPU.  You cannot use Adélie Linux on a POWER4 or earlier CPU.  The Freescale e5500/e6500 and SPE processors do not have the required AltiVec/VMX extensions, and therefore will also not be able to run the 64-bit Adélie Linux.</para>
        <para>At least 3 GB of disk space is required for a full desktop installation of Adélie Linux.  The base server installation of Adélie Linux requires about 200 MB of disk space, not counting additional packages and your data.  It is recommended that you have at least 1 GB of disk space for most server roles.</para>
    </section>
    <section>
        <title>AArch64 (64-bit ARM)</title>
        <para>All known 64-bit ARM processors should be able to run Adélie Linux.</para>
    </section>
</chapter>
